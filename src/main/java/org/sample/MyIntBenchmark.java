package org.sample;

import java.util.Arrays;

import org.openjdk.jmh.annotations.Benchmark;

/**
 * A demo benchmark. It was generated with
 * 
 * <pre>
 * $ mvn archetype:generate \
 *         -DinteractiveMode=false \
 *         -DarchetypeGroupId=org.openjdk.jmh \
 *         -DarchetypeArtifactId=jmh-java-benchmark-archetype \
 *         -DgroupId=org.sample \
 *         -DartifactId=test \
 *         -Dversion=1.0
 * </pre>
 * 
 * Build with
 * 
 * <pre>
 * mvn clean install
 * </pre>
 * 
 * To speed up the test, run with
 * 
 * <pre>
 * java -jar target/benchmarks.jar -f 2 -i 5 -wi 5
 * </pre>
 */
public class MyIntBenchmark {
    @Benchmark
    public int testRandomInts(IntArrays arrays) {
        return sumPositiveInts(arrays.randomInts);
    }

    @Benchmark
    public int testSortedInts(IntArrays arrays) {
        return sumPositiveInts(arrays.sortedInts);
    }

    @Benchmark
    public int testAllOnes(IntArrays arrays) {
        return sumPositiveInts(arrays.allOnes);
    }

    @Benchmark
    public int testAllPositive(IntArrays arrays) {
        return sumPositiveInts(arrays.allPositive);
    }

    private int sumPositiveInts(int[] array) {
        int sum = 0;
        for (int i : array) {
            if (i > 0) {
                sum += i;
            }
        }
        return sum;
    }

    private enum Testcase {
        ALL, RANDOM, SORTED, ALL_ONES, ALL_POSITIVE
    }

    public static void main(String[] args) {
        if (args.length != 1) {
            printHelpAndExit();
        }
        try {
            MyIntBenchmark benchmark = new MyIntBenchmark();
            benchmark.run(Testcase.valueOf(args[0]));
        } catch (IllegalArgumentException e) {
            printHelpAndExit();
        }
    }

    private static void printHelpAndExit() {
        System.out.println("Provide exactly one argument, one of " + Arrays.toString(Testcase.values()));
        System.exit(0);
    }

    private void run(Testcase testcase) {
        IntArrays arrays = new IntArrays();
        if (testcase == Testcase.ALL) {
            runAll(arrays);
        } else {
            runBenchmark(arrays, testcase);
        }
    }

    private void runBenchmark(IntArrays arrays, Testcase testcase) {
        int[][] testcaseArrays = new int[][] { null, arrays.randomInts, arrays.sortedInts, arrays.allOnes,
            arrays.allPositive };
        int[] array = testcaseArrays[testcase.ordinal()];
            
        long start = System.currentTimeMillis();
        int sum = 0;
        for (int i = 0; i < 100_000; i++) {
            sum += sumPositiveInts(array);
        }
        System.out
                .println("testcase: " + testcase + "; time: " + (System.currentTimeMillis() - start) + "; sum: " + sum);
    }

    private void runAll(IntArrays arrays) {
        for (int i = 1; i < Testcase.values().length; i++) {
            runBenchmark(arrays, Testcase.values()[i]);
        }
    }
}
/*
 * Sample output from running with Java 8 or Core2 Duo:
 *  kofa@eagle:~/git/jmh-sample/target/classes > java -cp . org.sample.MyBenchmark ALL
 *  testcase: RANDOM; time: 14848; sum: 37643168
 *  testcase: SORTED; time: 15053; sum: 37643168
 *  testcase: ALL_ONES; time: 14924; sum: 1410065408
 *  testcase: ALL_POSITIVE; time: 15027; sum: -1461823968
 *  
 * So they are the same (slow).
 *  
 *  Individually:
 *  kofa@eagle:~/git/jmh-sample/target/classes > java -cp . org.sample.MyBenchmark RANDOM
 *  testcase: RANDOM; time: 14823; sum: 37643168
 *  kofa@eagle:~/git/jmh-sample/target/classes > java -cp . org.sample.MyBenchmark SORTED
 *  testcase: SORTED; time: 14634; sum: 37643168
 *  kofa@eagle:~/git/jmh-sample/target/classes > java -cp . org.sample.MyBenchmark ALL_ONES
 *  testcase: ALL_ONES; time: 6800; sum: 1410065408
 *  kofa@eagle:~/git/jmh-sample/target/classes > java -cp . org.sample.MyBenchmark ALL_POSITIVE
 *  testcase: ALL_POSITIVE; time: 6718; sum: -1461823968
 * 
 * ALL_ONES and ALL_POSITIVE are fast (branch prediction?); RANDOM is slow; SORTED should be close to the fast ones, but is slow, too.
 * 
 * Running via JMH as described in Javadoc at top, on the same machine, same VM:
 *  # Run complete. Total time: 00:01:24
 *  
 *  Benchmark                     Mode  Cnt      Score      Error  Units
 *  MyBenchmark.testAllOnes      thrpt   10  13699.624 ± 1070.857  ops/s
 *  MyBenchmark.testAllPositive  thrpt   10  13370.762 ± 1606.316  ops/s
 *  MyBenchmark.testRandomInts   thrpt   10   6444.614 ±  452.766  ops/s
 *  MyBenchmark.testSortedInts   thrpt   10   6437.622 ±  472.039  ops/s
 * 
 * Very noticable difference between testAllOnes/testAllPositive and testRandomInts/testSortedInts;
 * behaviour is like the 'hand-made' test with individual runs.
 */