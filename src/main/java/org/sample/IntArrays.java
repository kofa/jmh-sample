/**
 * 
 */
package org.sample;

import java.util.Arrays;
import java.util.Random;

import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.State;

/**
 * @author kofa
 *
 */
@State(Scope.Benchmark)
public class IntArrays {
    private static final int SIZE = 100_000;
    private final Random random = new Random(0);
    public final int[] randomInts = randomInts();
    public final int[] sortedInts = sortedInts();
    public final int[] allOnes = allOnes();
    public final int[] allPositive = allPositive();

    private int[] randomInts() {
        int[] array = new int[SIZE];
        for (int i = 0; i < SIZE; i++) {
            array[i] = random.nextInt();
        }
        return array;
    }

    private int[] sortedInts() {
        int[] array = Arrays.copyOf(randomInts, SIZE);
        Arrays.sort(array);
        return array;
    }

    private int[] allOnes() {
        int[] array = new int[SIZE];
        for (int i = 0; i < SIZE; i++) {
            array[i] = 1;
        }
        return array;
    }
    private int[] allPositive() {
        int[] array = new int[SIZE];
        for (int i = 0; i < SIZE; i++) {
            array[i] = random.nextInt(Integer.MAX_VALUE);
        }
        return array;
    }
}
