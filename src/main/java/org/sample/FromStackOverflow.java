package org.sample;
import java.util.Arrays;
import java.util.Random;

/*
 * From http://stackoverflow.com/questions/11227809/why-is-it-faster-to-process-a-sorted-array-than-an-unsorted-array,
 * with minor differences.
 * Sample output with Core2 Duo, with Arrays.sort commented out:
 * 12.059206914
 * sum = 155184200000
 * With Arrays.sort active:
 * 4.17073188
 * sum = 155184200000
 */
public class FromStackOverflow
{
    private static final int SIZE = 100_000;
    private static final int data[] = new int[SIZE];

    public static void main(String[] args)
    {
        Random rnd = new Random(0);
        for (int c = 0; c < SIZE; ++c) {
            data[c] = rnd.nextInt() % 256;
        }
        testUnsorted();
        testSorted();
    }
    
    private static void testUnsorted() {
        // !!! With this, the next loop runs faster
        //Arrays.sort(data);
    
        // Test
        long sum = test();
        System.out.println("unsorted sum = " + sum);
    }
    
    private static void testSorted() {
        // !!! With this, the next loop runs faster
        Arrays.sort(data);
    
        // Test
        long sum = test();
        System.out.println("sorted sum = " + sum);
    }

    private static long test() {
        long start = System.nanoTime();
        long sum = 0;
    
        for (int i = 0; i < 100_000; ++i)
        {
            // Primary loop
            for (int c = 0; c < SIZE; ++c)
            {
                if (data[c] >= 128)
                    sum += data[c];
            }
        }
    
        System.out.println((System.nanoTime() - start) / 1000_000_000.0);
        return sum;
    }
    
}